package com.example.upload_image.Service.ServiceImp;

import com.example.upload_image.Model.FileUpload;
import com.example.upload_image.Repository.FileUploadRepository;
import com.example.upload_image.Service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class FileUploadServiceImp implements FileUploadService {

    @Autowired
    private FileUploadRepository fileUploadRepository;
    @Override
    public FileUpload save(FileUpload fileUpload) {
        return fileUploadRepository.save(fileUpload);
    }

    @Override
    public List<FileUpload> findAll() {
        return fileUploadRepository.findAll();
    }

    @Override
    public FileUpload findOne(String id) {
        return findOne(id);
    }

    @Override
    public FileUpload update(String id, FileUpload fileUpload) {
        return null;
    }

    @Override
    public void delete(String id) {

    }
}
