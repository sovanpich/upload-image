package com.example.upload_image.Service;

import com.example.upload_image.Model.FileUpload;

import java.util.List;

public interface FileUploadService {

    public FileUpload save(FileUpload fileUpload);
    public List<FileUpload> findAll();
    public FileUpload findOne(String id);
    public FileUpload update(String id,FileUpload fileUpload);
    public void delete(String id);
}
