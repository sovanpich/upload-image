package com.example.upload_image.Repository;

import com.example.upload_image.Model.FileUpload;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileUploadRepository extends JpaRepository<FileUpload,String> {
    FileUpload findFileUploadById (Long id);
}
